// Preloader 
jQuery(document).ready(function () {
    //Preloader
   
   jQuery(document).ready(function(){
        jQuery('#preloader').remove();
       
    });
/**** owl carousel ****/

    var owl = jQuery("#benefit-demo");
    owl.owlCarousel({
        itemsCustom: [
                [767, 1]
                , [992, 1]
                , [1200, 1]
                , [1500, 1]]
        , navigation: false
        , pagination: true
        , slideSpeed: 1000
        , scrollPerPage: true
        , autoPlay:true
    });
    
    
    
      jQuery('#pricing-slider').owlCarousel({
      autoplay: 3000,
      loop: true,
      margin: 17,
      responsiveClass: true,
      responsive: {
          0: {
              items: 3,
              nav: true
          },
          600: {
              items: 3,
              nav: true
          },
          1000: {
              items: 3,
              nav: true,
          }
      }
  });
    
    // Wow 
wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: false, // default
    live: true // default
})
wow.init();
    // Contact Page 
    
     jQuery(".form_control1").on("focusout load", function() {
    if (jQuery(this).val() != "") {
      jQuery(this).siblings("label").addClass("has-content");
    } else {
      jQuery(this).siblings("label").removeClass("has-content");
    }
  });
   
    // Select placehodler color
    
     jQuery('select').change(function () {
            if (jQuery(this).children('option:first-child').is(':selected')) {
                jQuery(this).addClass('placeholder1');
            }
            else {
                jQuery(this).removeClass('placeholder1');
            }
        });
});


